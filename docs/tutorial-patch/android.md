---
sidebar_position: 1
title: Android
description: Patch an Anime Game with LSPatch
---

# Patching an Anime Game with LSPatch

## Introduction

This tutorial will show you how to patch an Anime Game with LSPatch.

## LSPatch APK (Root/ADB)

:::tip
Choose one of the following methods to run a Shizuku service on your device
:::

### Setup

#### Root

* First, install [Shizuku] on your device.
* Compile a [YuukiPS Module][YuukiPS_Module] with your PC/Laptop, and copy or move it to your device after compiling. 
  OR you can download a YuukiPS Module that I have already [compiled][Compiled]. 
* Install [LSPatch] on your device. 
* Open Shizuku and choose `Start (for rooted devices)`. 
  ![Shizuku](img/LSPatch_APK/shizuku_root.png)
* If it asks for root permission, allow it. 
  ![Root](./img/LSPatch_APK/SuperUser_Request.png)
* Wait until Shizuku has started. 
  ![Shizuku](./img/LSPatch_APK/process.png)

#### ADB

* First, install [Shizuku] on your device.
* Compile a [YuukiPS Module][YuukiPS_Module] with your PC/Laptop, and copy or move it after compiling to your device.
* Install [LSPatch] on your device.
* Make sure your device is connected to your PC/Laptop.
* First, go to Settings, Developer Options, and enable `USB Debugging`.
* Open Command Prompt or Terminal and type `adb devices`.
* If your device is connected, you will see something like this:

    ```bash
    List of devices attached

    emulator-5554   device
    ```

* Now, enter or copy this command to your terminal:

    ```bash copy
    adb shell sh /storage/emulated/0/Android/data/moe.shizuku.privileged.api/start.sh
    ```

* If the output is `info: shizuku_starter exit with 0`, it means that Shizuku has started.

### Patching

* After starting the Shizuku service, go to `Authorized (?) applications` and choose `LSPatch`.
![Shizuku](./img/LSPatch_APK/authorized.png)

* Open LSPatch and navigate to the `Manage` section.
![LSPatch](./img/LSPatch_APK/manage.png)

* Click on the `+` sign in the bottom right corner.

:::warning
Make sure that Genshin Impact is not already patched by LSPatch before patching it again, otherwise, it will cause a crash or error.
:::

* Choose either `Select apk(s) from storage` or `Select an installed app`, but we will choose `Select an installed app` because it's easier.

* Select `Genshin Impact`.

* For `Patched Mode`, choose `Integrated`.
![LSPatch](./img/LSPatch_APK/patched_mode.png)

* Click on `Embed modules`.
![LSPatch](./img/LSPatch_APK/embed_module.jpg)

* Choose a `YuukiPS` module.
![LSPatch](./img/LSPatch_APK/choose_module.jpg)

* Click on `Start Patch` in the bottom right corner.

* After successful patching, you can install it. But make sure you have already backed up your game data.
![LSPatch](./img/LSPatch_APK/patched.png)

## LSPatch command line (Linux/Windows/Termux)

First, download the [LSPatch jar][LSPatch_jar] and the [YuukiPS Module][YuukiPS_Module].

:::warning
Make sure that your Genshin Impact game is not already patched by LSPatch before patching it again. Otherwise, it may cause a crash or an error.
:::

:::tip
Choose one of the following methods to patch an Anime Game for your operating system:
:::

### Linux

* Get the path of your `Genshin Impact` APK. For example: `/home/user/Downloads/Genshin Impact.apk`
* Get the path of your `YuukiPS Module`. For example: `/home/user/Downloads/YuukiPS.apk`
* If you haven't installed Java yet, install it with version 17 or higher.

JRE:

```bash
sudo apt install openjdk-17-jre
```

JDK:

```bash
sudo apt install openjdk-17-jdk
```

* Now open your terminal and type this command:

```bash
java -jar /home/user/Downloads/lspatch.jar /home/user/Downloads/"Genshin Impact.apk" -m /home/user/Downloads/YuukiPS.apk
```

* After successfully patching, you can copy or move the patched APK to your device and install it. However, make sure you have already backed up your game data.

### Windows

* Get the path of your `Genshin Impact` APK. For example: `C:\Users\user\Downloads\Genshin Impact.apk`
* Get the path of your `YuukiPS Module`. For example: `C:\Users\user\Downloads\YuukiPS.apk`
* If you haven't installed Java yet, install it with version 17 or higher from [here](https://www.oracle.com/java/technologies/downloads/#jdk17-windows).

* Now open your Command Prompt and type this command:

```batch
java -jar C:\Users\user\Downloads\lspatch.jar "C:\Users\user\Downloads\Genshin Impact.apk" -m C:\Users\user\Downloads\YuukiPS.apk
```

* After successfully patching, you can copy or move the patched APK to your device and install it. However, make sure you have already backed up your game data.

### Termux

* Get the path of your `Genshin Impact` APK. For example: `/sdcard/Download/Genshin Impact.apk`
* Get the path of your `YuukiPS Module`. For example: `/sdcard/Download/YuukiPS.apk`
* If you haven't installed Java yet, install it with version 17:

```bash
apt install openjdk-17
```

* Now open your Termux and type this command:

```bash
java -jar /sdcard/Download/lspatch.jar /sdcard/Download/"Genshin Impact.apk" -m /sdcard/Download/YuukiPS.apk
```

* After successfully patching, you can install the APK. However, make sure you have already backed up your game data.

Alternatively, you can use [PatchGenshinAPK] to patch your Genshin Impact APK.

<!-- Link -->
[YuukiPS_Module]: https://github.com/YuukiPS/Launcher-Android

[Compiled]: https://elaxan.com/download/Genshin-Android/yuuki.yuukips.apk

[Shizuku]: https://play.google.com/store/apps/details?id=moe.shizuku.privileged.api

[LSPatch]: https://github.com/LSPosed/LSPatch/releases

[LSPatch_jar]: https://github.com/LSPosed/LSPatch/releases/download/v0.5.1/lspatch.jar

[PatchGenshinAPK]: https://github.com/Score-Inc/PatchGenshinAPK
