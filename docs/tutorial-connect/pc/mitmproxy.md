---
sidebar_position: 4
---

# mitmproxy

Guide to use mitmproxy for connecting to Private Server

(For Advanced Users)

:::info
From version 2.7 and up [Requires Patch](/docs/intro)
:::

## Download

- Download mitmproxy from [here](https://mitmproxy.org/)

## Installing

1. Install mitmproxy and run as _administrator_\* (*optional if the proxy doesn't work properly)

    You will see a console running

    ![img](./img/console-mitm.png)

    And allow firewall access (If you didn't it won't work)

    :::note
    If the app didn't run, please see [troubleshooting](/docs/troubleshooting/pc/mitmproxy-troubleshoot)
    :::

2. Change windows proxy to `127.0.0.1` and `8080` (default port for mitm)

    Open windows settings -> Proxy
    ![img](./img/proxy-win.png)

    Note that if you have other proxy running this will not work, please only use one of your choice
    ![img](./img/set-proxy-win.png)

    After that you will see traffic flows
    ![img](./img/traffic-flow.png)

3. Head over to [mitm](https://mitm.it) and download the mitmproxy CA certificate

    The site can only be accessed if you have mitm running and the proxy configured, if you can't access the site make sure you have done step 1 - 3
    ![img](./img/down-cert-mitm.png)

    Install the certificate
    > You can either try the automatic installation or the manual if you don't know how

    If you see a "Security Warning" window, click on Yes (Otherwise it will not work)

    ![img](./img/trust-cert-mitm.png)

4. You're all set, now you can close the mitmproxy and turn off your windows proxy (If you want to continue later, if not you can skip **Turning off windows proxy**)*

    :::note
    You won't be able to connect to the internet unless you have the proxy off. See [Troubleshooting](/docs/troubleshooting/pc/mitmproxy-troubleshoot#my-pc-cant-connect-to-the-internet) for more info.
    :::

## Running

1. Create a folder (Optional)

2. Create a file named `proxy_config.py` and paste this code :

    ```py
    import os

    # This can also be replaced with another IP address.
    USE_SSL = True
    REMOTE_HOST = "login.yuuki.me"
    REMOTE_PORT = 443
    
    if os.getenv('MITM_REMOTE_HOST') != None:
        REMOTE_HOST = os.getenv('MITM_REMOTE_HOST')
    if os.getenv('MITM_REMOTE_PORT') != None:
        REMOTE_PORT = int(os.getenv('MITM_REMOTE_PORT'))
    if os.getenv('MITM_USE_SSL') != None:
        USE_SSL = bool(os.getenv('MITM_USE_SSL'))

    print('MITM Remote Host: ' + REMOTE_HOST)
    print('MITM Remote Port: ' + str(REMOTE_PORT))
    print('MITM Use SSL ' + str(USE_SSL))
    ```

3. Create another file named `proxy.py` and paste this code :

    ```py
    import collections
    import random
    from mitmproxy import http, connection, ctx, tls
    from abc import ABC, abstractmethod
    from enum import Enum
    from mitmproxy.utils import human
    from proxy_config import USE_SSL
    from proxy_config import REMOTE_HOST
    from proxy_config import REMOTE_PORT

    class MlgmXyysd_Animation_Company_Proxy:

        LIST_DOMAINS = [
            "api-os-takumi.mihoyo.com",
            "hk4e-api-os-static.mihoyo.com",
            "hk4e-sdk-os.mihoyo.com",
            "dispatchosglobal.yuanshen.com",
            "osusadispatch.yuanshen.com",
            "account.mihoyo.com",
            "log-upload-os.mihoyo.com",
            "dispatchcntest.yuanshen.com",
            "devlog-upload.mihoyo.com",
            "webstatic.mihoyo.com",
            "log-upload.mihoyo.com",
            "hk4e-sdk.mihoyo.com",
            "api-beta-sdk.mihoyo.com",
            "api-beta-sdk-os.mihoyo.com",
            "cnbeta01dispatch.yuanshen.com",
            "dispatchcnglobal.yuanshen.com",
            "cnbeta02dispatch.yuanshen.com",
            "sdk-os-static.mihoyo.com",
            "webstatic-sea.mihoyo.com",
            "webstatic-sea.hoyoverse.com",
            "hk4e-sdk-os-static.hoyoverse.com",
            "sdk-os-static.hoyoverse.com",
            "api-account-os.hoyoverse.com",
            "hk4e-sdk-os.hoyoverse.com",
            "overseauspider.yuanshen.com",
            "gameapi-account.mihoyo.com",
            "minor-api.mihoyo.com",
            "public-data-api.mihoyo.com",
            "uspider.yuanshen.com",
            "sdk-static.mihoyo.com",
            "abtest-api-data-sg.hoyoverse.com",
            "log-upload-os.hoyoverse.com"
        ]

        def request(self, flow: http.HTTPFlow) -> None:
            if flow.request.host in self.LIST_DOMAINS:
                if USE_SSL:
                    flow.request.scheme = "https"
                else:
                    flow.request.scheme = "http"
                flow.request.host = REMOTE_HOST
                flow.request.port = REMOTE_PORT

    class InterceptionResult(Enum):
        SUCCESS = 1
        FAILURE = 2
        SKIPPED = 3

    class TlsStrategy(ABC):
        def __init__(self):
            self.history = collections.defaultdict(lambda: collections.deque(maxlen=200))

        @abstractmethod
        def should_intercept(self, server_address: connection.Address) -> bool:
            raise NotImplementedError()

        def record_success(self, server_address):
            self.history[server_address].append(InterceptionResult.SUCCESS)

        def record_failure(self, server_address):
            self.history[server_address].append(InterceptionResult.FAILURE)

        def record_skipped(self, server_address):
            self.history[server_address].append(InterceptionResult.SKIPPED)

    class ConservativeStrategy(TlsStrategy):
        def should_intercept(self, server_address: connection.Address) -> bool:
            return InterceptionResult.FAILURE not in self.history[server_address]

    class ProbabilisticStrategy(TlsStrategy):
        def __init__(self, p: float):
            self.p = p
            super().__init__()

        def should_intercept(self, server_address: connection.Address) -> bool:
            return random.uniform(0, 1) < self.p

    class MaybeTls:
        strategy: TlsStrategy

        def load(self, l):
            l.add_option(
                "tls_strategy", int, 0,
                "TLS passthrough strategy. If set to 0, connections will be passed through after the first unsuccessful "
                "handshake. If set to 0 < p <= 100, connections with be passed through with probability p.",
            )

        def configure(self, updated):
            if "tls_strategy" not in updated:
                return
            if ctx.options.tls_strategy > 0:
                self.strategy = ProbabilisticStrategy(ctx.options.tls_strategy / 100)
            else:
                self.strategy = ConservativeStrategy()

        def tls_clienthello(self, data: tls.ClientHelloData):
            server_address = data.context.server.peername
            if not self.strategy.should_intercept(server_address):
                ctx.log(f"TLS passthrough: {human.format_address(server_address)}.")
                data.ignore_connection = True
                self.strategy.record_skipped(server_address)

        def tls_established_client(self, data: tls.TlsData):
            server_address = data.context.server.peername
            ctx.log(f"TLS handshake successful: {human.format_address(server_address)}")
            self.strategy.record_success(server_address)

        def tls_failed_client(self, data: tls.TlsData):
            server_address = data.context.server.peername
            ctx.log(f"TLS handshake failed: {human.format_address(server_address)}")
            self.strategy.record_failure(server_address)
            
    addons = [
        MlgmXyysd_Animation_Company_Proxy(),
        MaybeTls()
    ]
    ```

4. Make sure both `proxy_config.py` and `proxy.py` are in the same path (location)

5. Now open command prompt / terminal on where both files are located

    ![img](./img/example-folder.png)

    and type

    ```bash
    mitmdump.exe -s proxy.py -k
    ```

    Then do step 2 from [Installing](#installing)

6. Run the game
