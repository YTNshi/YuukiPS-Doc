---
sidebar_position: 2
--- 

# YuukiPS Launcher

Guide to use YuukiPS Launcher for connecting to Private Server

## Why YuukiPS Launcher ?

It has:

- Automatic game detection (if you have an official launcher).
- Download Metadata and UserAssembly patch or original file based on the current game version or by selecting a folder (RSA patch soon).
- Verify Game Data and Patches based on MD5 matched from server check API.
- Direct Patches (offline mode) using a key that supports Metadata & UserAssembly.
- Auto-rollback to the original version, so you can play the official version without renaming it again.
- Local proxy support, so no need for another proxy daemon running.
- Server list support.
- Cheat Multi (Acrepi, Minty, Bcrepi, Bkebi, Cotton, and more) (soon).
- Other games like SR support (soon).
- Auto-update.

## Requirements

- [.NET Runtime Desktop (6.0)](https://dotnet.microsoft.com/en-us/download/dotnet/6.0/runtime)

:::info
Epic Games version of the game is not supported
:::

## Video Guide

- https://youtu.be/PgC9DGo8Np4 - Yuuki ver. (Unavailable)
- https://youtu.be/eN_Ur5Obhzo - TheScore ver. (**Available**)
- https://youtu.be/7yyN2qsMR_0 - Kwelp ver. (**Available**)

## Downloading

1. Get the latest version from [here](https://github.com/YuukiPS/Launcher-PC/releases).

2. Download `YuukiPS.zip`. Make sure you download the latest version.

![img](./img/download-launcher.png)

## Running

1. Extract the zip to a folder.

2. Run `YuukiPS.exe` with administrator privileges. If the app doesn't run, see [troubleshooting](/docs/troubleshooting/pc/yuukips-launcher-troubleshoot#when-launching-the-app-it-instantly-closed).

3. Set the game path on the config tab, and then click Save.

    ![img](./img/game-path.png)

    :::note
    If you see "No game file config found", please go to [troubleshooting](/docs/troubleshooting/pc/yuukips-launcher-troubleshoot#no-game-file-config-found).
    :::

4. Select a server, and then launch.

    ![img](./img/launch-game.png)

5. When you see a security warning, click on Yes. If you don't trust the CA, the launcher proxy won't work. (Unless you're using a separate proxy daemon, you can set this in the launcher settings by toggling off the **Enable** option.)

    ![img](./img/trust-cert.png)

    And allow firewall access.

    ![img](./img/allow-firewall.png)

6. Wait until the game launches.
