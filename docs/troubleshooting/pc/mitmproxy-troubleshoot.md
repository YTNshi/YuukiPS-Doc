---
sidebar_position: 4
---

# mitmproxy

Known issue for mitmproxy and how to solve them

## Common Issue

List of Common Issue and how to solve it

### When launching the app it instantly closed

If you see a console showing and it says:

```ps
cannot be loaded because running scripts is disabled on this system. For more information, see about_Execution_Policies at https:/go.microsoft.com/fwlink/?LinkID=135170.
+ CategoryInfo          : SecurityError: (:) [], ParentContainsErrorRecordException
+ FullyQualifiedErrorId : UnauthorizedAccess
```

:::note
(The console usually instantly closed after you opened the app so you won't be able to see what it says if you have the termination behaviour to automatic, you can set this on terminal settings)
:::

To solve it, please select a solution below :

- Manual Fix (For Advanced)

1. Open powershell as administrator
2. Type `Get-ExecutionPolicy` to check the current ExecutionPolicy applied
3. If it says `Restricted` change it to `RemoteSigned` by using the `Set-ExecutionPolicy`. Read <u><b>[here](https://www.sharepointdiary.com/2014/03/fix-for-powershell-script-cannot-be-loaded-because-running-scripts-is-disabled-on-this-system.html)</b></u> for more info

- Using Windows Settings (**Recommended**)

1. Open windows settings and find Powershell (Make sure you have enabled Developer Mode)

    For Windows 11, `Privacy & security -> For developers -> Powershell`

    For Windows 10, `Update & security -> For developers -> Powershell`

2. Enable `Change execution policy to allow local powershell scripts to run without signing. Require signing for remote scripts.`

    ![img](./img/powershell-settings.png)

### My PC can't connect to the Internet

As i told in the [Tutorial](/docs/tutorial-connect/pc/mitmproxy.md#installing) step 4. That **You won't be able to connect to the internet unless you have the proxy off**. Make sure both of the mitm and windows proxy are turned on and working, and if you are done, do the oposite.

## Proxy Issue

List of Proxy Issue and how to solve them

### Client TLS handshake failed

If you get this error when running mitm :

```ps
Client TLS handshake failed. The client does not trust the proxy's certificate for example.domain.com (OpenSSL Error([('SSL routines', '', 'sslv3 alert certificate unknown')]))
```

It means you haven't trusted / installed the mitmproxy CA certificate. In order to fix this issue, head over to [mitm](https://mitm.it) and install the mitmproxy certiicate by reading the instruction provided there or read the [Installing Guide](/docs/tutorial-connect/pc/mitmproxy.md#installing) on step 3.

### Errno 10048

If you get this error when trying to run mitm :

```ps
HTTP(S) proxy failed to listen on *:PORTNUMBER with [Errno 10048] error while attempting to bind on address ('0.0.0.0', PORTNUMBER): only one usage of each socket address (protocol/network address/port) is normally permitted
Try specifying a different port by using `--mode regular@8081`.
```

It means the port is either being used by another instance of mitm, or other application that is running and using this port. To fix it, simply check the app that is using the port and close it. Or you can start mitm with other port by using the `-p PORTNUMBER` command-line when launching it.

:::note
You will have to set your windows proxy port the same as the mitm port listening
:::
