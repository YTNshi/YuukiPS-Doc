---
title: "Error Codes"
sidebar_position: 2
description: "List of common error codes"
---
# 4214

If you're trying to connect to Official Server, it's not allowed to connect to Official Server using apk launcher mod because libil2cpp.so is patched

If you're trying to connect to Private Server, it's because proxy failed to change a URL to Private Server. You can try to reopen the game and try again.
