export type StarRail = StarRailData[];

export interface StarRailData {
    id: number;
    platfrom: string;
    version: Version[];
}

export interface Version {
    name: string;
    project: Project;
    download: Download[];
}

export interface Download {
    link: Link[];
}

export interface Link {
    name: string;
    url_name?: string;
    url?: string;
    comment: string;
    game?: Game;
    url_direct?: string;
    package?: Package;
    mirror?: Mirror[];
}

export interface Game {
    Full: Full;
    Update: any[];
}

export interface Full {
    path: string;
    path_direct: string;
    size: string;
    md5: string;
    entry: string;
    voice_packs: VoicePack[];
    decompressed_path: string;
    segments: any[];
    package_size: string;
}

export interface VoicePack {
    language: string;
    path: string;
    path_direct: string;
    size: string;
    md5: string;
    package_size: string;
}

export interface Mirror {
    name: string;
    url: string;
    url_direct: string;
}

export interface Package {
    name: string;
    md5: string;
    region: string;
}

export interface Project {
    name: string;
    name_engine: string;
    engine_url: string;
    short: string;
    id: number;
}
