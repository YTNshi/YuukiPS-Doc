export type Root = Root2[];

export interface Root2 {
    website_url: string;
    contributors: Contributor[];
}

export interface Contributor {
    nickname: string;
    id: string;
    profile_picture_url: string;
}