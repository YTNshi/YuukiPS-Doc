export type DownloadGI = DownloadGIData[];

export interface DownloadGIData {
    id: number;
    platfrom: string;
    version: Version[];
}

export interface Version {
    name: string;
    project: Project;
    download: Download[];
}

export interface Download {
    link: Link[];
}

export interface Link {
    name: string;
    comment: string;
    game?: Game;
    url_name?: string;
    url?: string;
    url_direct?: string;
    mirror_text?: string;
    mirror?: Mirror[];
    package?: Package;
}

export interface Game {
    Full: Full;
    Update: any[];
}

export interface Full {
    path: string;
    path_direct: string;
    size: string;
    md5: string;
    entry: string;
    voice_packs: any[];
    decompressed_path: string;
    segments: Segment[];
    package_size: string;
}

export interface Segment {
    path: string;
    md5: string;
    package_size: string;
    path_direct?: string;
}

export interface Mirror {
    name: string;
    url: string;
    url_direct: string;
}

export interface Package {
    name: string;
    md5: string;
    region: string;
}

export interface Project {
    name: string;
    name_engine: string;
    engine_url: string;
    short: string;
    id: number;
}

export interface Language {
    [key: string]: string;
}
