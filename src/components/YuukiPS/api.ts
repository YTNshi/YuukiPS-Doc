import { returnApplyTheCommand, Account, Player } from '../../types/api';

const timeoutInSeconds = 30;

const removeLeadingSymbol = (input: string): string => {
    const symbolRegex = /^[^A-Za-z0-9]+/;
    const match = input.match(symbolRegex);

    if (match && match.index === 0) {
        return input.slice(match[0].length);
    }

    return input;
}

const generateErrorMessage = (error: any): { message: string, retcode: number } => {
    const errorMessage = error instanceof Error ? error.message : String(error);
    return { message: errorMessage, retcode: 500 };
}

export default class YuukiPS {
    public static apiUrlAccount = 'https://ps.yuuki.me/api/v2/account';

    public static apiUrlCommand = 'https://ps.yuuki.me/api/v2/server/:server/command';

    /**
     * Apply command to YuukiPS API
     *
     * @param {string} uid - UID of the player
     * @param {string | number} code - Code of the player
     * @param {string} server - Server of the player
     * @param {string} cmd - Command to apply
     * @returns {Promise<returnApplyTheCommand>} Result of the command
     */
    public static async applyCommand(
        uid: string,
        code: string | number,
        server: string,
        cmd: string,
    ): Promise<returnApplyTheCommand> {
        if (!uid || !server || !cmd) {
            return { message: 'Invalid input parameters', retcode: 400 };
        }

        const command = removeLeadingSymbol(cmd);
        const apiUrl = `${this.apiUrlCommand}`.replace(':server', server);
        const requestBody = {
            uid,
            server,
            code,
            cmd: command,
        };
        try {
            const response = await fetch(apiUrl, {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(requestBody),
            });
            const resultFromAPI = await response.json();

            if (resultFromAPI) {
                return resultFromAPI;
            } else {
                return { message: "Can't Access the server", retcode: 404 };
            }
        } catch (error) {
            return generateErrorMessage(error);
        }
    }

    /**
     * Check account information
     *
     * @param {string | number} uid - UID of the player
     * @param {string | number} code - Code of the player
     * @returns {Promise<Player[]>} Array of player data
     */
    public static async checkAccount(uid: string | number, code: string | number): Promise<Player[]> {
        const account: Account = await fetch(this.apiUrlAccount, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                uid: `${uid}`,
                code: `${code}`,
            }),
        }).then((response) => response.json());

        if (!account.data) {
            throw new Error(`Failed to get account information [${account.message}]`);
        }

        return account.data.player.filter((item) => {
            if (item.data?.data) {
                return item;
            }
        });
    }
}