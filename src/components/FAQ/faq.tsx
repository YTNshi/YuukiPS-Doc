import React, {useState} from 'react';
import Head from '@theme/Heading'
import Tag from '@theme/Tag'
import Link from '@docusaurus/Link'
import './styles.css'

// TODO: Code block with copy button
// TODO: Improve Tag List (make it simple)

export interface FaqProps {
    id: number;
    title: string;
    description: string;
    tag: string[];
}

const DescriptionConverter = ({ data }: { data: FaqProps[] }) => {
    const convertDescriptionToHTML = (description: string) => {
        const parts = description.split(/{{(.*?)}}/);
        return parts.map((part, index) => {
            if (index % 2 === 0) {
                return <span style={{whiteSpace: 'pre-wrap'}}>{part}</span>;
            } else {
                const [type, content] = part.split(': ');
                if (type === 'cb') {
                    return <code>{content}</code>;
                } else if (type === 'hl') {
                    const [text, link] = content.split(' | ');
                    return (
                        <Link href={link} target={'_blank'} rel={'noopener noreferrer'}>
                            {text}
                        </Link>
                        // <a href={link} target='_blank' rel='noopener noreferrer'>
                        //     {text}
                        // </a>
                    );
                } else if (type === 'sc') {
                    const [link] = content.split('|');
                    if (!link) throw new Error('Scroll link is not defined');
                    const linkedItem = data.find((value) => value.id === parseInt(link, 10));
                    const title = linkedItem ? linkedItem.title : 'No Title found';
                    return (
                        <>
                            {title && (
                                <Tag permalink={`#${convertID(title)}`} label={title}/>
                                // <button
                                //     onClick={() =>
                                //         scroller.scrollTo(link.trim(), { smooth: true, offset: -300, duration: 500 })
                                //     }
                                // >
                                //     {title}
                                // </button>
                            )}
                        </>
                    );
                } else if (type === 'tb') {
                    const rows = content.split(' || ');
                    const headers = rows[0].split(' | ');
                    const dataRows = rows.slice(1);

                    return (
                        <table>
                            <thead>
                            <tr>
                                {headers.map((header, index) => (
                                    <th key={index}>{header}</th>
                                ))}
                            </tr>
                            </thead>
                            <tbody>
                            {dataRows.map((dataRow, rowIndex) => (
                                <tr key={rowIndex}>
                                    {dataRow.split(' | ').map((cell, cellIndex) => (
                                        <td key={cellIndex}>{cell}</td>
                                    ))}
                                </tr>
                            ))}
                            </tbody>
                        </table>
                    )
                }
            }
            return null;
        });
    };

    const [filteredData, setFilteredData] = useState<FaqProps[]>(data);
    const [searchQuery, setSearchQuery] = useState('');

    const handleTagClick = (tag: string) => {
        if (tag === 'all') {
            setFilteredData(data);
        } else {
            const filteredItems = data.filter((value: FaqProps) => value.tag.includes(tag));
            setFilteredData(filteredItems);
        }
    };
    const handleSearch = (query) => {
        setSearchQuery(query);
        const filteredItems = data.filter((value) => {
            const tagsMatch = value.tag.some(tag => tag.includes(query));
            const titleMatch = value.title.toLowerCase().includes(query.toLowerCase());
            const descriptionMatch = value.description.toLowerCase().includes(query.toLowerCase());
            return tagsMatch || titleMatch || descriptionMatch;
        });
        setFilteredData(filteredItems);
    };

    const convertID = (title: string): string => {
        return title.toLowerCase().replace(/[^a-z0-9]+/g, '_');
    }

    const uniqueTags = [...new Set(data.flatMap(item => item.tag))].sort();

    return (
        <div>
            <div className="tag-buttons">
                <button
                    className="tag-button"
                    onClick={() => handleTagClick('all')}
                >
                    All
                </button>
                {uniqueTags.map((tag, tagIndex) => (
                    <button
                        key={`unique-button-${tagIndex}`}
                        className="tag-button"
                        onClick={() => handleTagClick(tag)}
                    >
                        {tag.toUpperCase()}
                    </button>
                ))}
            </div>
            <hr />
            <div className="search-bar">
                <input
                    type="text"
                    placeholder="Search..."
                    value={searchQuery}
                    onChange={(e) => handleSearch(e.target.value)}
                />
            </div>
            {filteredData.map((item, index) => (
                <>
                    <div className="faq-item" key={`main-div-${index}`}>
                        {item.tag.map((tag, tagIndex) => (
                            <button
                                key={`tag-key-${tagIndex}`}
                                className="tag-button"
                                type='button'
                                onClick={() => handleTagClick(tag)}
                            >
                                {tag.toUpperCase()}
                            </button>
                        ))}
                        <Head as={'h2'} id={convertID(item.title)}>
                            {item.title}
                        </Head>
                        <p className="item-description">
                            {convertDescriptionToHTML(item.description)}
                        </p>
                    </div>
                </>
            ))}
        </div>
    );
};
export default DescriptionConverter;
