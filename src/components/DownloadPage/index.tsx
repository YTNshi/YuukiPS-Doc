import React, { useEffect, useState } from 'react';
import Loading from '@theme/Loading';
import './styles.css';
import { Version, Language, DownloadGI, Link } from '../../types/downloadGI';
import { StarRail, Version as StarRailVersion, Link as SRLink, VoicePack } from '../../types/downloadSR';
import Tabs from '@theme/Tabs';
import TabItem from '@theme/TabItem';

const Pages = ({ platform, type, game }: { platform: 'Android' | 'PC'; type?: 'data' | 'app'; game?: 'gi' | 'sr' }) => {
    const [loadingPage, setLoadingPage] = useState<boolean>(true);
    const [APIDownload, setAPIDownload] = useState<(Version | StarRailVersion)[]>();
    const [language, setLanguage] = useState<Language>();
    const [isErrorFetch, setIsErrorFetch] = useState<boolean>(false);
    const [errorMessage, setErrorMessage] = useState<any>();

    useEffect(() => {
        const url = () => {
            return 'https://ps.yuuki.me/json/:type_game/download.json'.replace(':type_game', game === 'sr' ? 'star-rail' : 'genshin-impact');
        };
        fetch(url())
            .then((res) => res.json())
            .then((value: DownloadGI | StarRail) => {
                value.filter((getVersion) => {
                    if (getVersion.platfrom === platform) {
                        setAPIDownload(getVersion.version);
                    }
                });
            })
            .catch((error) => {
                console.error(error);
                setIsErrorFetch(true);
                setErrorMessage(error);
            });
        fetch('https://raw.githubusercontent.com/YuukiPS/Core-Language/main/en_US.json')
            .then((res) => res.json())
            .then((value: Language) => {
                setLanguage(value);
                setLoadingPage(false);
            })
            .catch((error) => {
                console.error(error);
                setIsErrorFetch(true);
                setErrorMessage(error);
            });
    }, []);

    const formatBytes = (bytes: number, decimals: number = 2): string => {
        if (bytes === 0) return '0 Bytes';

        const k = 1024;
        const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];

        const i = Math.floor(Math.log(bytes) / Math.log(k));

        return parseFloat((bytes / Math.pow(k, i)).toFixed(decimals)) + ' ' + sizes[i];
    };

    const DownloadAndroidAPKGI = () => {
        return (
            <>
                {APIDownload &&
                    APIDownload.map((version: Version) => (
                        <div className={'block-download-link'}>
                            <h3>
                                {version.name} ({version.project.name})
                            </h3>
                            {version.download[0].link.map(
                                (link, index) =>
                                    index === 0 && (
                                        <div className={'block-download-link'}>
                                            <p>{link.name}</p>
                                            {link.package && (
                                                <>
                                                    <p>Package: {link.package.name}</p>
                                                    <p>MD5: {link.package.md5}</p>
                                                    <p>Region: {link.package.region}</p>
                                                </>
                                            )}
                                            <div>
                                                <div className={'mirror-button-style'}>{link.url_name || 'Official'}</div>
                                                <a href={link.url_direct}>
                                                    <div className={'button-style'}>{link.name}</div>
                                                </a>
                                                {link.mirror && (
                                                    <>
                                                        {link.mirror.map((mirrorLink, index) => (
                                                            <>
                                                                {index === 0 && (
                                                                    <>
                                                                        <br />
                                                                        <div className={'mirror-button-style'}>Mirror</div>
                                                                    </>
                                                                )}
                                                                <a href={mirrorLink.url_direct}>
                                                                    <div className={`button-style ${index === link.mirror!.length - 1 ? 'last-button' : ''}`}>{mirrorLink.name}</div>
                                                                </a>
                                                            </>
                                                        ))}
                                                    </>
                                                )}
                                            </div>
                                        </div>
                                    )
                            )}
                        </div>
                    ))}
            </>
        );
    };

    const DownloadAndroidAPKSR = () => {
        return (
            <>
                {APIDownload &&
                    APIDownload.map((version) => (
                        <div className={'block-download-link'}>
                            <h3>
                                {version.name} ({version.project.name})
                            </h3>
                            {version.download[0].link.map((link: SRLink, index: number) => (
                                <div className={'block-download-link'} key={`star-rail-download-link-${index}`}>
                                    <p>{link.name}</p>
                                    {link.package && (
                                        <>
                                            <p>Package: {link.package.name}</p>
                                            <p>MD5: {link.package.md5}</p>
                                            <p>Region: {link.package.region}</p>
                                        </>
                                    )}
                                    <div>
                                        <div className={'mirror-button-style'}>{link.url_name || 'Official'}</div>
                                        <a href={link.url_direct}>
                                            <div className={'button-style'}>{link.name}</div>
                                        </a>
                                        {link.mirror && (
                                            <>
                                                {link.mirror.map((mirrorLink, index) => (
                                                    <>
                                                        {index === 0 && (
                                                            <>
                                                                <br />
                                                                <div className={'mirror-button-style'}>Mirror</div>
                                                            </>
                                                        )}
                                                        <a href={mirrorLink.url_direct}>
                                                            <div className={`button-style ${index === link.mirror!.length - 1 ? 'last-button' : ''}`}>{mirrorLink.name}</div>
                                                        </a>
                                                    </>
                                                ))}
                                            </>
                                        )}
                                    </div>
                                </div>
                            ))}
                        </div>
                    ))}
            </>
        );
    };

    const isGILink = (link: any): link is import('../../types/downloadGI').Link => {
        return (link as import('../../types/downloadGI').Link).game?.Full.path !== undefined;
    };

    const getFileNameURL = (url: string): string | null => {
        const parts = url.split('/');

        const fileName = parts[parts.length - 1];

        if (fileName.trim() !== '') {
            return fileName
        }
        return null
    }

    const DownloadPC = () => {
        return (
            <>
                {APIDownload &&
                    APIDownload.map((value) => (
                        <div className={'block-download-link'} style={{ padding: '20px', margin: '10px', border: '1px solid #ccc', borderRadius: '5px' }}>
                            <h2>
                                {value.name} ({value.project.name})
                            </h2>
                            {value.download[0].link.map((link: Link) => (
                                <div className={'block-download-link'} style={{ marginTop: '15px' }}>
                                    <p>{link.name}</p>
                                    <p
                                        dangerouslySetInnerHTML={{
                                            __html: link.comment,
                                        }}
                                    ></p>
                                    {link.url && (
                                        <div className='block-download-link'>
                                            {link.url_name && <p>{link.url_name}</p>}
                                            <a href={link.url} className='button-style' style={{ marginBottom: '10px' }}>
                                                Download
                                            </a>
                                        </div>
                                    )}
                                    {isGILink(link) && (
                                        <>
                                            <p>Size: {formatBytes(parseInt(link.game?.Full.size || '0'))}</p>
                                            <p>MD5: {link.game?.Full.md5 || 'Unknown'}</p>
                                            {link.game?.Full.segments && (
                                                <>
                                                    {link.game.Full.segments.length > 0 && (
                                                        <div style={{ marginTop: '10px', padding: '10px', borderRadius: '5px' }}>
                                                            <h4>Download Parts</h4>
                                                            <Tabs>
                                                                {link.game.Full.segments.map((segment, index) => (
                                                                    <TabItem value={`Part ${index + 1}`}>
                                                                        <p>Name File: {getFileNameURL(segment.path_direct || segment.path)}</p>
                                                                        <p>MD5: {segment.md5}</p>
                                                                        <p>Size: {formatBytes(parseInt(segment.package_size))}</p>
                                                                        <a href={segment.path_direct || segment.path}
                                                                           className="button-style">
                                                                            Download
                                                                        </a>
                                                                    </TabItem>
                                                                ))}
                                                            </Tabs>
                                                        </div>
                                                    )}
                                                    {link.game && link.game.Full.voice_packs.length > 0 && (
                                                        <div className={'line-download-link'}>
                                                            <h2>Voice Packs</h2>
                                                            <Tabs>
                                                                {link.game.Full.voice_packs.map((voicePack: VoicePack, index: number) => (
                                                                    <TabItem>
                                                                        <p>MD5: {voicePack.md5.toUpperCase()}</p>
                                                                        <p>Size: {formatBytes(parseInt(voicePack.size))}</p>
                                                                        <p>Package Size: {formatBytes(parseInt(voicePack.package_size))}</p>
                                                                        <a href={voicePack.path}>
                                                                            <div className={'button-style'}>Download</div>
                                                                        </a>
                                                                    </TabItem>
                                                                ))}
                                                            </Tabs>
                                                        </div>
                                                    )}
                                                </>
                                            )}
                                        </>
                                    )}
                                </div>
                            ))}
                        </div>
                    ))}
            </>
        );
    };

    return (
        <>
            {APIDownload && (
                <>
                    <Loading isLoading={loadingPage} timedOut={true} error={errorMessage} pastDelay={false} retry={() => {}} />
                    {platform === 'Android' && type === 'app' && (game === 'gi' ? <DownloadAndroidAPKGI /> : <DownloadAndroidAPKSR />)}
                    {platform === 'PC' && <DownloadPC />}
                </>
            )}
            {isErrorFetch && (
                <div className={'theme-admonition theme-admonition-warning alert alert--danger admonition_node_modules-@docusaurus-theme-classic-lib-theme-Admonition-styles-module'}>
                    <div className={'admonitionContent_node_modules-@docusaurus-theme-classic-lib-theme-Admonition-styles-module'}>
                        An error occurred while fetching the data. Please check the console for more details and report this issue to our Discord Server.
                    </div>
                </div>
            )}
        </>
    );
};

export default Pages;
