---
sidebar_position: 3
---

# Fiddler Classic

Panduan untuk menggunakan Fiddler untuk terhubung ke Private Server
:::info
Genshin Impact: versi 2.7 dan seterusnya [Memerlukan Patch](/docs/intro)<br/>
Star Rail: tidak perlu patch, hanya perlu proxy.
:::

## Unduh

- Unduh dan instal Fiddler Classic dari [sini](https://file2.yuuki.me/Local_EU/Project/GenshinImpact/Tool/FiddlerSetup.exe).

## Menjalankan

1. Jalankan Fiddler Classic dengan hak administrator.

2. Percayai Sertifikat Root Fiddler Classic. Jika Anda tidak mempercayainya, proxy tidak akan berfungsi. Anda dapat mengatur ini**\***

   ![img](./img/cert-warning.png)

   > Tools -> Options -> HTTPS -> Actions -> Trust Root Certificate -> Toggle Decrypt HTTPS traffic on

3. Tempelkan ini ke tab FiddlerScript Anda.

   ```cs
   import System;
   import System.Windows.Forms;
   import Fiddler;
   import System.Text.RegularExpressions;
   class Handlers
   {
       static function OnBeforeRequest(oS: Session) {
           if(
               oS.host.EndsWith(".yuanshen.com") ||
               oS.host.EndsWith(".hoyoverse.com") ||
               oS.host.EndsWith(".mihoyo.com") ||
               oS.host.EndsWith(".zenlesszonezero.com") ||
               oS.host.EndsWith(".honkaiimpact3.com") ||
               oS.host.EndsWith(".bhsr.com") ||
               oS.host.EndsWith(".starrails.com") ||
               oS.uriContains("http://overseauspider.yuanshen.com:8888/log")
            ) {
                 oS.host = "ps.yuuki.me";
            }
       }
   };
   ```

   ![img](./img/fiddler-script.png)

4. Simpan skrip.

   ![img](./img/save-script.png)

5. Jalankan game
