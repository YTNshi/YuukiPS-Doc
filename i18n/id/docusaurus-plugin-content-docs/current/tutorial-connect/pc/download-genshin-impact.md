---
title: Unduh Genshin Impact
sidebar_position: 1
description: Halaman unduhan ini untuk versi PC Genshin Impact
---

# Unduh

:::info
Halaman unduhan ini untuk versi PC.<br/>
Jika Anda tidak memiliki klien game atau terlalu lambat untuk diunduh dengan server resmi, Anda dapat mencoba ini.
:::

## Pembaruan Game (hdiff)

:::note
Memerlukan Data Game Lengkap dari versi sebelumnya, contoh: 3.4 memerlukan 3.3 terlebih dahulu.
:::

### 3.6.0 > 3.7.0 (OS)

[Official][Official_hdiff_3.6.0_3.7.0] | EU1 | SG1 | GD1-1 | GD1-2 | GD2 | GD3 | OD1 | Terabox<br/>
(MD5: F9C4F21FA785FBC48845C44B13472B9E)

### 3.5.0 > 3.7.0 (OS)

[Official][Official_hdiff_3.5.0_3.7.0] | EU1 | SG1 | GD1-1 | GD1-2 | GD2 | GD3 | OD1 | Terabox<br/>
(MD5: BCEC39F882910CDDDE95D05272DC7F69)

<details>

<summary>Archives only/ not supported</summary>

### 3.5.0 > 3.6.0 (OS)

[Official][Official_hdiff_3.5.0_3.6.0] | EU1 | SG1 | GD1-1 | GD1-2 | GD2 | GD3 | OD1 | Terabox<br/>
(MD5: CE07ABFA216757B35DA43401868DC65E)

### 3.4.0 > 3.6.0 (OS)

[Official][Official_hdiff_3.4.0_3.6.0] | EU1 | SG1 | GD1-1 | GD1-2 | GD2 | GD3 | OD1 | Terabox<br/>
(MD5: 69609F7A420B46EBE8DDD256625BED63)

### 3.3.0 > 3.5.0 (OS)

[Official][Official_hdiff_3.3.0_3.5.0] | EU1 | SG1 | GD1-1 | GD1-2 | GD2 | GD3 | OD1 | Terabox<br/>
(MD5: DA20792BCFCF890583F66EFBC5A6B5E4)

### 3.3.0 > 3.4.0 (OS)

[Official][Official_hdiff_3.3.0_3.4.0] | EU1 | SG1 | GD1-1 | GD1-2 | GD2 | GD3 | OD1 | Terabox<br/>
(MD5: D88BF205205500A02D677A668ED63DC4)

### 3.2.0 > 3.4.0 (OS)

[Official][Official_hdiff_3.2.0_3.4.0] | EU1 | SG1 | GD1-1 | GD1-2 | GD2 | GD3 | OD1 | Terabox<br/>
(MD5: 528D54CB8D09A887807A7941C1062186)

### 3.3.50 aka 3.4 Beta (OS)

Official | EU1 | SG1 | GD1-1 | [GD1-2][GD1_hdiff_3.3.0_3.3.50] | GD2 | GD3 | OD1 | Terabox<br/>
(MD5: 1B1F39D43DE717A6EFAE502B104FBAAE)

</details>

## Full Game Data

### 3.7 (OS)

[Official][Official_full_3.7.0] | EU1 | SG1 | GD1-1 | [GD1-2][GD1_full_3.7.0] | GD2 | GD3 | OD1 | Terabox<br/>
(MD5: f4c838b84ffa21a62f8e3ff5c2af7c7b)

### 3.6 (OS)

[Official][Official_full_3.6.0] | EU1 | SG1 | GD1-1 | [GD1-2][GD1_full_3.6.0] | GD2 | GD3 | OD1 | Terabox<br/>
(MD5: 2686795437c83749e975333216d99edf)

### 3.2 (OS)

[Official][Official_full_3.2.0] | EU1 | SG1 | GD1-1 | [GD1-2][GD1_full_3.2.0] | GD2 | GD3 | OD1 | Terabox<br/>
(MD5: affa3679ab20e4893e04088f5f44ecff)

<details>

<summary>Archives only/ not supported</summary>

### 3.5 (OS)

[Official][Official_full_3.5.0] | EU1 | SG1 | GD1-1 | [GD1-2][GD1_full_3.5.0] | GD2 | GD3 | OD1 | Terabox<br/>
(MD5: 1e6334da5268caf47e17d2fa12e9bef1)<br/>

### 3.4 (OS)

[Official][Official_full_3.4.0] | EU1 | SG1 | GD1-1 | [GD1-2][GD1_full_3.4.0] | GD2 | GD3 | OD1 | Terabox<br/>
(MD5: d431470c80f8f12c1381faa7cdc77a53)<br/>

### 3.3 (OS)

[Official][Official_full_3.3.0] | EU1 | SG1 | GD1-1 | [GD1-2][GD1_full_3.3.0] | GD2 | GD3 | OD1 | Terabox<br/>
(MD5: BF0D1A9187AA52F5F3E17BE26A263D33)<br/>
</details>

<!-- Game Data 3.7 OS -->
<!-- Full -->
[Official_full_3.7.0]: https://autopatchhk.yuanshen.com/client_app/download/pc_zip/20230513200104_2odHBzbUAP5IOIvE/GenshinImpact_3.7.0.zip
[GD1_full_3.7.0]: https://file2.yuuki.me/GD1/Project/GenshinImpact/Data/PC/3.7.0/Global/GenshinImpact_3.7.0.zip
<!-- hdiff -->
[Official_hdiff_3.6.0_3.7.0]: https://autopatchhk.yuanshen.com/client_app/update/hk4e_global/10/game_3.6.0_3.7.0_hdiff_SIQuq5CfVPEXnBNy.zip
[Official_hdiff_3.5.0_3.7.0]: https://autopatchhk.yuanshen.com/client_app/update/hk4e_global/10/game_3.5.0_3.7.0_hdiff_fy67SAIbUXdo1kMp.zip

<!-- Game Data 3.6 OS -->
<!-- Full -->
[Official_full_3.6.0]: https://autopatchhk.yuanshen.com/client_app/download/pc_zip/20230331200258_xR748JQdRJ19pQH0/GenshinImpact_3.6.0.zip
[GD1_full_3.6.0]: https://file2.yuuki.me/GD1/Project/GenshinImpact/Data/PC/3.6.0/Global/GenshinImpact_3.6.0.zip
<!-- hdiff -->
[Official_hdiff_3.4.0_3.6.0]: https://autopatchhk.yuanshen.com/client_app/update/hk4e_global/10/game_3.4.0_3.6.0_hdiff_t30d4K7DUnwNcmja.zip
[Official_hdiff_3.5.0_3.6.0]: https://autopatchhk.yuanshen.com/client_app/update/hk4e_global/10/game_3.5.0_3.6.0_hdiff_70c4ojbL1mfTwY3Q.zip

<!-- Game Data 3.5 -->
<!-- Full -->
[Official_full_3.5.0]: https://autopatchhk.yuanshen.com/client_app/download/pc_zip/20230220120928_iIYV9krGiWL06eeB/GenshinImpact_3.5.0.zip
[GD1_full_3.5.0]: https://file2.yuuki.me/GD1/Project/GenshinImpact/Data/PC/3.5.0/Global/GenshinImpact_3.5.0.zip
<!-- hdiff -->
<!-- [Official_hdiff_3.4.0_3.5.0]: https://autopatchhk.yuanshen.com/client_app/update/hk4e_global/10/game_3.4.0_3.5.0_hdiff_rXYAZvelstu54E2c.zip -->
[Official_hdiff_3.3.0_3.5.0]: https://autopatchhk.yuanshen.com/client_app/update/hk4e_global/10/game_3.3.0_3.5.0_hdiff_jFk1USEdyHepP3uG.zip

<!-- Game Data 3.4 -->
<!-- Full -->
[Official_full_3.4.0]: https://autopatchhk.yuanshen.com/client_app/download/pc_zip/20230109135018_10QhExKHwAoa4ecr/GenshinImpact_3.4.0.zip
[GD1_full_3.4.0]: https://file2.yuuki.me/GD1/Project/GenshinImpact/Data/PC/3.4.0/Global/GenshinImpact_3.4.0.zip
<!-- hdiff -->
[Official_hdiff_3.3.0_3.4.0]: https://autopatchhk.yuanshen.com/client_app/update/hk4e_global/10/game_3.3.0_3.4.0_hdiff_IxKGMXhCLzZAJqUm.zip
[Official_hdiff_3.2.0_3.4.0]: https://autopatchhk.yuanshen.com/client_app/update/hk4e_global/10/game_3.2.0_3.4.0_hdiff_2Tv5e1BCLFnW0dPG.zip

<!-- Game Data 3.3.5x -->
<!-- hdiff -->
[GD1_hdiff_3.3.0_3.3.50]: https://file2.yuuki.me/GD1/Project/GenshinImpact/Data/PC/3.3.50/Global/GenshinImpact_3.3.0_3.3.50_hdiff_SC1.rar

<!-- Game Data 3.3 -->
<!-- Full -->
[Official_full_3.3.0]: https://autopatchhk.yuanshen.com/client_app/download/pc_zip/20221128113321_cOH1qEM6iRagjJC6/GenshinImpact_3.3.0.zip
[GD1_full_3.3.0]: https://file2.yuuki.me/GD1/Project/GenshinImpact/Data/PC/3.3.0/Global/GenshinImpact_3.3.0.zip

<!-- Game Data 3.2 -->
<!-- Full -->
[Official_full_3.2.0]: https://autopatchhk.yuanshen.com/client_app/download/pc_zip/20221024103618_h2e3o3zijYKEqHnQ/GenshinImpact_3.2.0.zip
[GD1_full_3.2.0]: https://file2.yuuki.me/GD1/Project/GenshinImpact/Data/PC/3.2.0/Global/GenshinImpact_3.2.0.zip
