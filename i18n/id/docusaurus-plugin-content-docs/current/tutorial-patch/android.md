---
sidebar_position: 1
title: Android
description: Memasang Patch pada Game Anime dengan LSPatch
---

# Memasang Patch pada Game Anime dengan LSPatch

## Pendahuluan

Tutorial ini akan menunjukkan cara memasang patch pada Game Anime dengan LSPatch.

## LSPatch APK (Root/ADB)

:::tip
Pilih salah satu metode berikut untuk menjalankan layanan Shizuku pada perangkat Anda
:::

### Persiapan

#### Root

* Pertama, instal [Shizuku] pada perangkat Anda.
* Kompilasikan [Modul YuukiPS][YuukiPS_Module] dengan PC/Laptop Anda, dan salin atau pindahkan ke perangkat Anda setelah dikompilasi.
  ATAU Anda dapat mengunduh Modul YuukiPS yang telah saya [kompilasi][Compiled].
* Instal [LSPatch] pada perangkat Anda.
* Buka Shizuku dan pilih `Start (untuk perangkat yang sudah di-root)`.
  ![Shizuku](img/LSPatch_APK/shizuku_root.png)
* Jika diminta izin root, izinkan.
  ![Root](./img/LSPatch_APK/SuperUser_Request.png)
* Tunggu hingga Shizuku telah dimulai.
  ![Shizuku](./img/LSPatch_APK/process.png)

#### ADB

* Pertama, instal [Shizuku] pada perangkat Anda.
* Kompilasikan [Modul YuukiPS][YuukiPS_Module] dengan PC/Laptop Anda, dan salin atau pindahkan ke perangkat Anda setelah dikompilasi.
* Instal [LSPatch] pada perangkat Anda.
* Pastikan perangkat Anda terhubung ke PC/Laptop Anda.
* Pertama, buka Pengaturan, Opsi Pengembang, dan aktifkan `Debugging USB`.
* Buka Command Prompt atau Terminal dan ketik `adb devices`.
* Jika perangkat Anda terhubung, Anda akan melihat sesuatu seperti ini:

    ```bash
    List of devices attached

    emulator-5554   device
    ```

* Sekarang, masukkan atau salin perintah ini ke terminal Anda:

    ```bash copy
    adb shell sh /storage/emulated/0/Android/data/moe.shizuku.privileged.api/start.sh
    ```

* Jika keluarannya adalah `info: shizuku_starter exit with 0`, itu berarti Shizuku telah dimulai.

### Memasang Patch

* Setelah memulai layanan Shizuku, buka `Authorized (?) applications` dan pilih `LSPatch`.
![Shizuku](./img/LSPatch_APK/authorized.png)

* Buka LSPatch dan navigasikan ke bagian `Manage`.
![LSPatch](./img/LSPatch_APK/manage.png)

* Klik tanda `+` di sudut kanan bawah.

:::warning
Pastikan Genshin Impact belum dipasang patch oleh LSPatch sebelum memasangnya lagi, jika tidak, akan menyebabkan crash atau error.
:::

* Pilih `Select an installed app`.

* Pilih `Genshin Impact`.

* Untuk `Patched Mode`, pilih `Integrated`.
![LSPatch](./img/LSPatch_APK/patched_mode.png)

* Klik `Embed modules`.
![LSPatch](./img/LSPatch_APK/embed_module.jpg)

* Pilih modul `YuukiPS`.
![LSPatch](./img/LSPatch_APK/choose_module.jpg)

* Klik `Start Patch` di sudut kanan bawah.

* Setelah berhasil dipasang patch, Anda dapat menginstalnya. Namun, pastikan Anda sudah mem-backup data game Anda.
![LSPatch](./img/LSPatch_APK/patched.png)

## LSPatch command line (Linux/Windows/Termux)

Pertama, unduh [LSPatch jar][LSPatch_jar] dan [Modul YuukiPS][YuukiPS_Module].

:::warning
Pastikan game Genshin Impact Anda belum dipasang patch oleh LSPatch sebelum memasangnya lagi. Jika tidak, akan menyebabkan crash atau error.
:::

:::tip
Pilih salah satu metode berikut untuk memasang patch pada Game Anime untuk sistem operasi Anda:
:::

### Linux

* Dapatkan jalur APK `Genshin Impact` Anda. Contohnya: `/home/user/Downloads/Genshin Impact.apk`
* Dapatkan jalur `Modul YuukiPS` Anda. Contohnya: `/home/user/Downloads/YuukiPS.apk`
* Jika Anda belum menginstal Java, instal dengan versi 17 atau yang lebih tinggi.

JRE:

```bash
sudo apt install openjdk-17-jre
```

JDK:

```bash
sudo apt install openjdk-17-jdk
```

* Sekarang buka terminal Anda dan ketik perintah ini:

```bash
java -jar /home/user/Downloads/lspatch.jar /home/user/Downloads/"Genshin Impact.apk" -m /home/user/Downloads/YuukiPS.apk
```

* Setelah berhasil dipasang patch, Anda dapat menyalin atau memindahkan APK yang sudah dipasang patch ke perangkat Anda dan menginstalnya. Namun, pastikan Anda sudah mem-backup data game Anda.

### Windows

* Dapatkan jalur APK `Genshin Impact` Anda. Contohnya: `C:\Users\user\Downloads\Genshin Impact.apk`
* Dapatkan jalur `Modul YuukiPS` Anda. Contohnya: `C:\Users\user\Downloads\YuukiPS.apk`
* Jika Anda belum menginstal Java, instal dengan versi 17 atau yang lebih tinggi dari [sini](https://www.oracle.com/java/technologies/downloads/#jdk17-windows).

* Sekarang buka Command Prompt Anda dan ketik perintah ini:

```batch
java -jar C:\Users\user\Downloads\lspatch.jar "C:\Users\user\Downloads\Genshin Impact.apk" -m C:\Users\user\Downloads\YuukiPS.apk
```

* Setelah berhasil dipasang patch, Anda dapat menyalin atau memindahkan APK yang sudah dipasang patch ke perangkat Anda dan menginstalnya. Namun, pastikan Anda sudah mem-backup data game Anda.

### Termux

* Dapatkan jalur APK `Genshin Impact` Anda. Contohnya: `/sdcard/Download/Genshin Impact.apk`
* Dapatkan jalur `Modul YuukiPS` Anda. Contohnya: `/sdcard/Download/YuukiPS.apk`
* Jika Anda belum menginstal Java, instal dengan versi 17:

```bash
apt install openjdk-17
```

* Sekarang buka Termux Anda dan ketik perintah ini:

```bash
java -jar /sdcard/Download/lspatch.jar /sdcard/Download/"Genshin Impact.apk" -m /sdcard/Download/YuukiPS.apk
```

* Setelah berhasil dipasang patch, Anda dapat menginstal APK. Namun, pastikan Anda sudah mem-backup data game Anda.

Anda juga dapat menggunakan [PatchGenshinAPK] untuk memasang patch pada APK Genshin Impact Anda.

<!-- Link -->
[YuukiPS_Module]: https://github.com/YuukiPS/Launcher-Android

[Compiled]: https://elaxan.com/download/Genshin-Android/yuuki.yuukips.apk

[Shizuku]: https://play.google.com/store/apps/details?id=moe.shizuku.privileged.api

[LSPatch]: https://github.com/LSPosed/LSPatch/releases

[LSPatch_jar]: https://github.com/LSPosed/LSPatch/releases/download/v0.5.1/lspatch.jar

[PatchGenshinAPK]: https://github.com/Score-Inc/PatchGenshinAPK
