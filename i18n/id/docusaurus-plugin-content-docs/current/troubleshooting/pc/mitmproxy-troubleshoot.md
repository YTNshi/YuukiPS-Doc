---
sidebar_position: 4
---

# mitmproxy

Masalah yang diketahui untuk mitmproxy dan cara memperbaikinya

## Masalah Umum

Daftar Masalah Umum dan cara memperbaikinya

### Ketika membuka aplikasi, aplikasi langsung tertutup

Jika anda melihat konsol muncul dan tertulis seperti ini:

```ps
cannot be loaded because running scripts is disabled on this system. For more information, see about_Execution_Policies at https:/go.microsoft.com/fwlink/?LinkID=135170.
+ CategoryInfo          : SecurityError: (:) [], ParentContainsErrorRecordException
+ FullyQualifiedErrorId : UnauthorizedAccess
```

:::note
(Konsol biasanya langsung tertutup setelah Anda membuka aplikasi sehingga Anda tidak akan dapat melihat apa yang tertulis jika Anda memiliki perilaku terminasi yang otomatis, Anda dapat mengatur ini pada pengaturan terminal)
:::

Untuk memperbaikinya, silakan pilih solusi di bawah ini:

- Perbaikan Manual (Untuk Pengguna Lanjutan)

1. Buka powershell sebagai administrator
2. Ketik `Get-ExecutionPolicy` untuk memeriksa ExecutionPolicy yang sedang berlaku
3. Jika tertulis `Restricted`, ubah menjadi `RemoteSigned` dengan menggunakan `Set-ExecutionPolicy`. Baca <u><b>[di sini](https://www.sharepointdiary.com/2014/03/fix-for-powershell-script-cannot-be-loaded-because-running-scripts-is-disabled-on-this-system.html)</b></u> untuk informasi lebih lanjut.

- Menggunakan Pengaturan Windows (**Disarankan**)

1. Buka pengaturan Windows dan cari Powershell (Pastikan Anda telah mengaktifkan Mode Pengembang)

    Untuk Windows 11, `Privasi & keamanan -> Untuk pengembang -> Powershell`

    Untuk Windows 10, `Pembaruan & keamanan -> Untuk pengembang -> Powershell`

2. Aktifkan `Change execution policy to allow local powershell scripts to run without signing. Require signing for remote scripts.`

    ![img](./img/powershell-settings.png)

### PC saya tidak dapat terhubung ke Internet

Seperti yang saya jelaskan pada langkah ke-4 di [Tutorial](/docs/tutorial-connect/pc/mitmproxy.md#installing), **Anda tidak akan dapat terhubung ke internet kecuali jika Anda mematikan proxy**. Pastikan kedua mitm dan proxy Windows dihidupkan dan berfungsi, dan jika Anda selesai, lakukan sebaliknya.

## Masalah Proxy

Daftar Masalah Proxy dan cara memperbaikinya

### Client TLS handshake failed

Jika Anda mendapatkan kesalahan ini saat menjalankan mitm:

```ps
Client TLS handshake failed. The client does not trust the proxy's certificate for example.domain.com (OpenSSL Error([('SSL routines', '', 'sslv3 alert certificate unknown')]))
```

Jika Anda mendapatkan kesalahan ini saat menjalankan mitm, itu berarti Anda belum mempercayai / menginstal sertifikat CA mitmproxy. Untuk memperbaiki masalah ini, kunjungi [mitm.it](https://mitm.it) dan instal sertifikat mitmproxy dengan membaca instruksi yang disediakan di sana atau baca [Panduan Instalasi](/docs/tutorial-connect/pc/mitmproxy.md#installing) pada langkah ke-3.

### Errno 10048

Jika anda mendapatkan kesalahan ini saat menjalankan mitm:

```ps
HTTP(S) proxy failed to listen on *:PORTNUMBER with [Errno 10048] error while attempting to bind on address ('0.0.0.0', PORTNUMBER): only one usage of each socket address (protocol/network address/port) is normally permitted
Try specifying a different port by using `--mode regular@8081`.
```

Ini berarti port tersebut sedang digunakan oleh instance mitm lain atau aplikasi lain yang sedang berjalan dan menggunakan port tersebut. Untuk memperbaikinya, cukup periksa aplikasi yang menggunakan port tersebut dan tutup aplikasi tersebut. Atau Anda dapat memulai mitm dengan port lain dengan menggunakan perintah `-p PORTNUMBER` saat meluncurkannya.

:::note
Anda harus mengatur port proxy Windows Anda sama dengan port listen mitm.
:::
