---
sidebar_position: 2
--- 

# YuukiPS Launcher

Masalah yang diketahui untuk YuukiPS Launcher dan cara memperbaikinya

## Ketika membuka aplikasi, aplikasi langsung tertutup

Ini terjadi ketika Anda belum menginstal [**.NET Runtime 6.0 (Desktop)**](https://dotnet.microsoft.com/en-us/download/dotnet/6.0/runtime). Untuk memperbaikinya, cukup instal dotnet (Run desktop apps). Pilih x64, x84 atau arm64 tergantung pada arsitektur CPU Anda. [**Klik di sini untuk cara memeriksa arsitektur CPU Anda**](https://pcguide101.com/cpu/what-is-my-processor-architecture/)

## No game file config found

![img](./img/no-game-config.png)

Ini terjadi ketika versi game Anda tidak didukung oleh launcher (artinya Anda memiliki versi game yang lebih baru atau klien yang tidak didukung oleh API MD5)

![img](./img/md5-error.png)

Periksa konsol dan kirim tangkapan layar ke saluran dukungan di [Discord](https://discord.gg/yuukips). <u>Tetapi pastikan untuk membaca sebelum memposting</u>, jika seseorang sudah memposting tentang hal itu, jangan posting lagi, cukup bergabunglah dalam percakapan, **gunakan fungsi pencarian**.
